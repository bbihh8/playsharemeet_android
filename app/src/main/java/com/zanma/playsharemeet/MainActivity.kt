package com.zanma.playsharemeet

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.zanma.playsharemeet.retrofit.Version
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"
    private val api_url = "http://playsharemeet.ddns.net/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        val listView:ListView = findViewById(R.id.regionsView)
//        val regionsList: List<String>
//        val regionsAdapter = RegionsAdapter(this, regionsList)
//        listView.adapter = regionsAdapter

        getServerApiVersion()
    }

    private fun getServerApiVersion() {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(api_url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val versionInterface = retrofit.create(Version::class.java)
        val versionCall = versionInterface.getVersion()

        versionCall.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                val versionResponse = response.body()
                Toast.makeText(
                    this@MainActivity, "Server API version is: "
                            + versionResponse, Toast.LENGTH_SHORT
                ).show()
                Log.d(TAG, versionResponse.toString())
            }
            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.e(TAG, "Error : $t")
            }
        })
    }
}
